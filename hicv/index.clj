([:html
  {:lang "en"}
  [:head
   [:meta {:charset "UTF-8"}]
   [:title "OnePager - One Page Responsive Portfolio Template"]
   [:meta
    {:content "width=device-width,initial-scale=1", :name "viewport"}]
   [:link
    {:media "screen", :href "styles/style.css", :rel "stylesheet"}]
   [:link {:href "styles/media-queries.css", :rel "stylesheet"}]
   [:link
    {:media "screen",
     :type "text/css",
     :href "./flex-slider/flexslider.css",
     :rel "stylesheet"}]
   [:link
    {:media "screen",
     :type "text/css",
     :rel "stylesheet",
     :href "styles/prettyPhoto.css"}]
   [:link
    {:media "screen",
     :type "text/css",
     :rel "stylesheet",
     :href "styles/tipsy.css"}]
   [:link
    {:type "text/css",
     :href "styles/politrend.css",
     :rel "stylesheet"}]
   [:script
    {:src "./scripts/jquery-1.7.1.min.js", :type "text/javascript"}]
   [:script
    {:src "./flex-slider/jquery.flexslider-min.js",
     :type "text/javascript"}]
   [:script
    {:type "text/javascript", :src "scripts/jquery.prettyPhoto.js"}]
   [:script {:type "text/javascript", :src "scripts/jquery.tipsy.js"}]
   [:script {:type "text/javascript", :src "scripts/jquery.knob.js"}]
   [:script
    {:src "./scripts/jquery.isotope.min.js", :type "text/javascript"}]
   [:script
    {:src "./scripts/jquery.smooth-scroll.min.js",
     :type "text/javascript"}]
   [:script
    {:src "./scripts/waypoints.min.js", :type "text/javascript"}]
   [:script {:src "./scripts/setup.js", :type "text/javascript"}]]
  [:body
   [:div#wrap
    "<!-- wrapper -->"
    [:div#sidebar
     "<!-- the  sidebar -->"
     "<!-- logo -->"
     [:a#logo
      {:href "#"}
      " "
      [:img
       {:alt "",
        :style "width: 93%",
        :src "./images/politrend_kz.png"}]]
     "<!-- navigation menu -->"
     [:ul#navigation
      [:li [:a.active {:href "#home"} "В начало"]]
      [:li [:a {:href "#about"} "О нас"]]
      [:li [:a {:href "#portfolio"} "Проекты"]]
      [:li [:a {:href "#skills"} "Наш уровень"]]
      [:li [:a {:href "#industries"} "Индустрия"]]
      [:li [:a {:href "#myclients"} "Наши клиенты"]]
      [:li [:a {:href "#contact"} "Контакты"]]]]
    [:div#container
     "<!-- page container -->"
     [:div#home.page
      "<!-- page home -->"
      [:div.page_content
       [:div.gf-slider
        "<!-- slider -->"
        [:ul.slides
         [:li
          " "
          [:img {:alt "", :src "images/01.jpg"}]
          [:p.flex-caption
           " Super-awesome simplistic-styled easily customizable one page scrolling portfolio\n                    template"]]
         [:li
          " "
          [:img {:alt "", :src "images/02.jpg"}]
          [:p.flex-caption
           " With Special Customization this template is perfect to be used as a blog template\n                    also"]]
         [:li
          " "
          [:img {:alt "", :src "images/03.jpg"}]
          [:p.flex-caption
           " Fully responsive layout supported on all devices including tablet and smartphones"]]
         [:li
          " "
          [:img {:alt "", :src "images/04.jpg"}]
          [:p.flex-caption
           " Purchase it now to grab it and use it on your work and make experiments with it"]]
         [:li
          " "
          [:img {:alt "", :src "images/05.jpg"}]
          [:p.flex-caption
           " Fully responsive layout supported on all devices including tablet and smartphones"]]]]
       [:div.space " "]
       [:div.clear " "]
       "<!-- services -->"
       [:div.one_half.first
        [:div.column_content
         [:h4 " Coded with Love!"]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "images/barcode72.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.one_half.last
        [:div.column_content
         [:h4 " Responsive Layout"]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "images/shipping72.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.space " "]
       [:div.one_half.first
        [:div.column_content
         [:h4 " Perfect for Portfolios"]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "./images/for-portfolio.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.one_half.last
        [:div.column_content
         [:h4 " Easily Customizable"]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "./images/customizable.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.space " "]
       [:div.one_half.first
        [:div.column_content
         [:h4 " Image Gallery "]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "./images/image-gallery.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.one_half.last
        [:div.column_content
         [:h4 " jQuery Powered"]
         [:img.left.no_border
          {:style
           "margin-top: 10px; margin-right: 10px",
           :alt "",
           :src "./images/jquery-code.png"}]
         [:p
          " "
          [:small
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at feugiat\n                    felis. Ut faucibus molestie turpis, sit amet scelerisque ipsum scelerisque quis.\n                    Quisque suscipit fermentum sodales."]]]]
       [:div.clear " "]]]
     [:div#about.page
      "<!-- page about -->"
      [:h3.page_title " About Us"]
      [:div.page_content
       [:p
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute\n              irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\n              pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia\n              deserunt mollit anim id est laborum."]
       [:h4.blue " Why Choose Us"]
       [:p
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute\n              irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\n              pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia\n              deserunt mollit anim id est laborum."]
       [:blockquote
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua.\n              "
        [:p
         " "
         [:small
          [:b "Sarfraz Shoukat"]
          " - Owner "
          [:a {:href "#"} "Greepit.com"]]]]
       [:div.clear " "]]]
     [:div#portfolio.page
      "<!-- page portfolio -->"
      [:h3.page_title " Portfolio"]
      [:div.page_content
       [:p
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
       [:ul#works_filter
        [:li [:a.selected {:data-filter "*", :href "#"} "Show All"]]
        [:li [:a {:data-filter ".css", :href "#"} "CSS"]]
        [:li [:a {:data-filter ".html_php", :href "#"} "HTML / PHP"]]
        [:li [:a {:data-filter ".js", :href "#"} "JavaScript"]]]
       [:div.clear " "]
       [:div#works
        "<!-- works -->"
        [:a
         {:href "images/photos/01.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.js {:alt "", :src "images/photos/01.jpg"}]
         " "]
        [:a
         {:href "images/photos/03.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.css {:alt "", :src "images/photos/03.jpg"}]
         " "]
        [:a
         {:href "images/photos/04.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.html_php {:alt "", :src "images/photos/04.jpg"}]
         " "]
        [:a
         {:href "images/photos/05.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.html_php {:alt "", :src "images/photos/05.jpg"}]
         " "]
        [:a
         {:href "images/photos/06.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.css {:alt "", :src "images/photos/06.jpg"}]
         " "]
        [:a
         {:href "images/photos/07.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.js {:alt "", :src "images/photos/07.jpg"}]
         " "]
        [:a
         {:href "images/photos/08.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.html_php {:alt "", :src "./images/photos/08.jpg"}]
         " "]
        [:a
         {:href "images/photos/09.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.js {:alt "", :src "images/photos/09.jpg"}]
         " "]
        [:a
         {:href "images/photos/10.jpg", :rel "prettyPhoto[gallery]"}
         " "
         [:img.work.html_php {:alt "", :src "images/photos/10.jpg"}]
         " "]
        " "]
       [:div.clear " "]]]
     [:div#skills.page
      "<!-- page skills -->"
      [:h3.page_title " Our Skills"]
      [:div.page_content
       [:div.one_fourth.first
        [:div.column_content
         [:h4.blue " Photoshop"]
         [:input.knob
          {:value "65",
           :data-bgcolor "#0d4667",
           :data-fgcolor "#cfdee7",
           :data-displayprevious "true",
           :data-angleoffset "0",
           :data-min "0",
           :data-width "120",
           :data-readonly "true"}]]]
       [:div.one_fourth
        [:div.column_content
         [:h4.blue " HTML5"]
         [:input.knob
          {:data-bgcolor "#0d4667",
           :data-fgcolor "#cfdee7",
           :value "45",
           :data-displayprevious "true",
           :data-angleoffset "0",
           :data-min "0",
           :data-width "120",
           :data-readonly "true"}]]]
       [:div.one_fourth
        [:div.column_content
         [:h4.blue " jQuery"]
         [:input.knob
          {:data-bgcolor "#0d4667",
           :data-fgcolor "#cfdee7",
           :value "85",
           :data-displayprevious "true",
           :data-angleoffset "0",
           :data-min "0",
           :data-width "120",
           :data-readonly "true"}]]]
       [:div.one_fourth.last
        [:div.column_content
         [:h4.blue " CSS3"]
         [:input.knob
          {:data-bgcolor "#0d4667",
           :data-fgcolor "#cfdee7",
           :value "95",
           :data-displayprevious "true",
           :data-angleoffset "0",
           :data-min "0",
           :data-width "120",
           :data-readonly "true"}]]]
       [:div.clear " "]]]
     [:div#industries.page
      "<!-- page industries -->"
      [:h3.page_title " Industries We Serve!"]
      [:div.page_content
       [:p
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
       [:div.space " "]
       [:div.clear " "]
       [:ul.sublist
        [:li [:a {:href "#"} "Freebies"]]
        [:li [:a {:href "#"} "Category Names"]]
        [:li [:a {:href "#"} "Graphic Design"]]
        [:li [:a {:href "#"} "Akay Akagunduz"]]
        [:li [:a {:href "#"} "News"]]
        [:li [:a {:href "#"} "Themeforest"]]
        [:li [:a {:href "#"} "Reviews"]]
        [:li [:a {:href "#"} "Links"]]
        [:li [:a {:href "#"} "Tutorials"]]
        [:li [:a {:href "#"} "Others"]]
        [:li [:a {:href "#"} "Web Development"]]]
       [:div.clear " "]]]
     [:div#myclients.page
      "<!-- page clients -->"
      [:h3.page_title " Our Clients"]
      [:div.page_content
       [:p
        " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
       [:div.space " "]
       [:div.clear " "]
       [:ul#clients.grid
        [:li.altoids
         [:a
          {:rel "altoids", :href "#"}
          " "
          [:img {:alt "Altoids", :src "images/clients/Altoids.png"}]]]
        [:li.facebook
         [:a
          {:rel "facebook", :href "#"}
          " "
          [:img
           {:alt "Facebook", :src "images/clients/Facebook.png"}]]]
        [:li.ge
         [:a
          {:rel "general-electric", :href "#"}
          " "
          [:img
           {:alt "General Electric", :src "images/clients/GE.png"}]]]
        [:li.orbit
         [:a
          {:rel "orbit", :href "#"}
          " "
          [:img {:alt "Orbit", :src "images/clients/orbitlogo.png"}]]]
        [:li.skittles
         [:a
          {:rel "skittles", :href "#"}
          " "
          [:img
           {:alt "Skittles", :src "images/clients/Skittles.png"}]]]
        [:li.jameson
         [:a
          {:rel "jameson", :href "#"}
          " "
          [:img {:alt "Jameson", :src "images/clients/Jameson.png"}]]]
        [:li.juicy_fruit
         [:a
          {:rel "juicy-fruit", :href "#"}
          " "
          [:img
           {:alt "Juicy Fruit",
            :src "images/clients/Juicy-Fruit.png"}]]]
        [:li.microsoft
         [:a
          {:rel "microsoft", :href "#"}
          " "
          [:img
           {:alt "Microsoft", :src "images/clients/Microsoft.png"}]]]
        [:li.a_e
         [:a
          {:rel "ae", :href "#"}
          " "
          [:img {:alt "A&E", :src "images/clients/AE.png"}]]]
        [:li.zynga
         [:a
          {:rel "zynga", :href "#"}
          " "
          [:img {:alt "Zynga", :src "images/clients/Zynga.png"}]]]
        [:li.smuin
         [:a
          {:rel "smuin", :href "#"}
          " "
          [:img {:alt "Smuin", :src "images/clients/Smuin.png"}]]]
        [:li.westfield
         [:a
          {:rel "westfield", :href "#"}
          " "
          [:img
           {:alt "Westfield", :src "images/clients/Westfield.png"}]]]]
       [:div.clear " "]]]
     [:div#contact.page
      "<!-- page contact -->"
      [:h3.page_title " Let's Get in Touch"]
      [:div.page_content
       [:fieldset#contact_form
        [:div#msgs " "]
        [:form#cform
         {:action "", :method "post", :name "cform"}
         [:input#name
          {:onblur "if(this.value == '') this.value = 'Full Name*'",
           :onfocus "if(this.value == 'Full Name*') this.value = ''",
           :value "Full Name*",
           :name "name",
           :type "text"}]
         [:input#email
          {:onblur
           "if(this.value == '') this.value = 'Email Address*'",
           :onfocus
           "if(this.value == 'Email Address*') this.value = ''",
           :value "Email Address*",
           :name "email",
           :type "text"}]
         [:input#subject
          {:onblur "if(this.value == '') this.value = 'Subject*'",
           :onfocus "if(this.value == 'Subject*') this.value = ''",
           :value "Subject*",
           :name "subject",
           :type "text"}]
         [:textarea#msg
          {:onblur "if(this.value == '') this.value = 'Your Message*'",
           :onfocus
           "if(this.value == 'Your Message*') this.value = ''",
           :name "message"}
          "Your Message*"]
         [:button#submit.button " Send Message"]]]
       [:div.clear " "]
       [:ul.social_icons
        [:li
         [:a#fb
          {:original-title "Join My Fan Club", :href "#"}
          " "
          [:img {:alt "Facebook", :src "images/facebook.png"}]]]
        [:li
         [:a#tw
          {:original-title "Follow Me on Twitter", :href "#"}
          " "
          [:img {:alt "Twitter", :src "images/twitter.png"}]]]
        [:li
         [:a#ld
          {:original-title "Find me on LinkedIn", :href "#"}
          " "
          [:img {:alt "LinkedIn", :src "images/linkedin.png"}]]]]]]
     [:div.footer
      [:p
       " © 2012 "
       [:a {:href "http://eGrappler.com"} "eGrappler.com"]
       ". Some Rights Reserved."]
      [:p
       " Designed With Love by "
       [:a {:href "http://esarfraz.com"} "Sarfraz Shoukat"]]]]]
   [:a.gotop {:href "#top"} "Top"]]])

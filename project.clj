(defproject politrend "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]

                 [ring "1.2.1"]
                 [ring/ring-json "0.3.0"]
                 
                 [compojure "1.1.6"]

                 ;;[enlive "1.1.5"]
                 [hiccup "1.0.4"]
                 [com.cemerick/friend "0.2.0"]

                 [overtone/at-at "1.2.0"]
                 [clj-time "0.6.0"]

                 [org.postgresql/postgresql "9.2-1002-jdbc4"]
                 [korma "0.3.0-RC6"]

                 [actus "0.1.0-SNAPSHOT"]

                 [com.draines/postal "1.11.1"]
                 ]

  :plugins [[lein-ring "0.8.10"]
            [hiccup-bridge "1.0.0-SNAPSHOT"] ;; конвертов в hiccup
            ]

  :ring {:handler politrend.handler/app
         :init politrend.handler/init
         :auto-reload? true
         :auto-refresh? false
         :nrepl {:start? true}}

  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]
                        [ring-serve "0.1.2"]]}})

;; example variant
;;(defproject app "0.1.0"
;;  :description "Trading digitalized"
;;  :url "https://app.com"
;;  :source-paths ["src-clj"]
;;  :dependencies [[org.clojure/clojure "1.4.0"]
;;                 [ring "1.1.6"]
;;                 [com.datomic/datomic-free "0.8.3538"]
;;                 [compojure "1.1.3"]
;;                 [hiccup "1.0.1"]
;;                 [clj-http "0.5.5"]
;;                 [cheshire "4.0.3"]]
;;  :plugins [[lein-ring "0.7.5"]
;;            [lein-cljsbuild "0.2.7"]]
;;  :ring {:handler app.server/handler
;;         :auto-reload? true
;;         :auto-refresh true}
;;  :cljsbuild {
;;    :builds [{:source-path "src-cljs"
;;              :compiler {:output-to "public/javascripts/main.js"
;;                         :optimizations :whitespace
;;                         :pretty-print true}}]}
;;  :profiles
;;    {:dev {:dependencies [[ring-mock "0.1.3"]]}})

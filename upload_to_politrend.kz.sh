#!/bin/sh

cd ../actus; git pull; lein install; cd ../politrend; git pull

lein ring uberwar

ssh -p 5578 politrend@vta.kz '/home/politrend/jetty/bin/jetty.sh stop'

scp -P 5578 target/politrend-0.1.0-SNAPSHOT-standalone.war politrend@politrend.kz:/home/politrend/jetty/webapps/politrend.war

ssh -p 5578 politrend@vta.kz '/home/politrend/jetty/bin/jetty.sh start'

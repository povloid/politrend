(ns politrend.service
  (:use korma.db)
  (:use korma.core)

  (:require [clj-time.core :as tco]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [clj-time.local :as tl]

            [actus.common-db-sql :as cdbsql]
            [actus.common-web :as cw]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])
            )
  )


;;**************************************************************************************************
;;* BEGIN Spec functions
;;* tag: <spec functions>
;;*
;;* description: Специальные функции
;;*
;;**************************************************************************************************

;;(def a "#home > Домой; #top > В начало")

;; (defn content-list-from-text [text]
;;   (->> text
;;        clojure.string/split-lines
;;        (map #(map clojure.string/trim (clojure.string/split % #">>" 2)) )))

(defn content-list-from-text [text]
  (letfn [(clean-str [s] (->> s (apply str) clojure.string/trim))]
    (let [[a, [i _ s]] (reduce
                        (fn [[a,[i t s]] c]
                          (cond (= c \;) [(conj a [(clean-str i),(clean-str s)]), [[] 0 []]]
                                (= c \>) [a, [i 1 s]]
                                (= t 0) [a, [(conj i c) 0 s]]
                                (= t 1) [a, [i 1 (conj s c)]]
                                ))
                        [[],[[] 0 []]]
                        text)]
      (if (empty? i) a
          (conj a [(clean-str i) (clean-str s)] )))))

;; END Spec functions
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Db settings
;;* tag: <db connect settings>
;;*
;;* description: Настройки соединения с базой данных
;;*
;;**************************************************************************************************

(def pg (postgres {:host "localhost"
                   :port 5433
                   :db "politrend"
                   :make-pool? true
                   :user "politrend"
                   :password "paradox"}))

(defdb korma-db pg)

(declare groups)

;; END Db settings
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Entitys
;;* tag: <entitys>
;;*
;;* description: Описание сущностей
;;*
;;**************************************************************************************************

;;------------------------------------------------------------------------------
;; BEGIN: Files
;; tag: <files>
;; description:
;;------------------------------------------------------------------------------

;; File save -----------------------------------------------------

(defentity files
  (pk :id))

(cdbsql/def-files-entitys-map)

;; END Files
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: News
;; tag: <news>
;; description: Новости
;;------------------------------------------------------------------------------

(defentity news
  (pk :id)
  (many-to-many files :news_files)

  (prepare #(->> %
                 (cdbsql/fields-transformation-clj-time->-sql-time [:cdate])
                 ))

  (transform #(->> %
                   (cdbsql/fields-transformation-clj-time-<-sql-time [:cdate])
                   ))
  )

(defn news-save [vals]
  (cdbsql/common-save-for-id news vals))

(def news-select
  (-> (select* news)) )

;;-----------------------------------------------------
;; BEGIN: news-files
;; tag: <news files>
;; description: Фалы для новостей
;;-----------------------------------------------------

(defentity news-files
  (table :news_files)
  (pk :id)
  (belongs-to files))

(cdbsql/def-files-entitys-map-add :news-files news-files :news_id)


;; END news-files
;;.....................................................
;; END News
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: Any content
;; tag: <any content>
;; description: Всякий контент
;;------------------------------------------------------------------------------

(defentity any-content
  (table :any_content)
  (pk :id))

(defn any-content-save [vals]
  (cdbsql/common-save-for-id any-content vals))

(def any-content-select
  (-> (select* any-content)))

(defn get-any-content [keyname]
  (select any-content (where (= :keyname (name keyname)))))

(defn get-any-content-title [keyname]
  (-> (select any-content (fields :title) (where (= :keyname (name keyname))))
      first :title))

(defn get-any-content-body [keyname]
  (-> (select any-content (fields :body) (where (= :keyname (name keyname))))
      first :body))

(defn get-any-as-map-k-* []
  (letfn [(add-k-v [a {keyname :keyname :as m} k]
            (assoc a
              (-> (str keyname "-" (name k)) keyword)
              (or (k m) "") ))]
    (->> (select any-content)
         (reduce #(-> %
                      (add-k-v %2 :title)
                      (add-k-v %2 :body))
                 {}))))


(defn save-any-content-row [keyname has-a-title editor description]
  (cdbsql/common-save-for-field any-content :keyname
                                {:hastitle has-a-title
                                 :keyname (name keyname)
                                 :editor editor
                                 :description description}))

(save-any-content-row :robots.txt false 0 "Фаил для поисковых роботов")
(save-any-content-row :about-as true 1 "блок: О нас")
(save-any-content-row :our-projects true 1 "блок: Наши проекты")
(save-any-content-row :our-clients true 1 "блок: Наши клиенты")
(save-any-content-row :footer-text false 1 "блок: Текст в подвале")
(save-any-content-row :contact-as true 1 "блок: Связаться с нами")

(save-any-content-row :title false 1 "мета тег в шаблоне")

(save-any-content-row :meta-author false 0 "мета тег в шаблоне")
(save-any-content-row :meta-copyright false 0 "мета тег в шаблоне")
(save-any-content-row :meta-region false 0 "мета тег в шаблоне")

;;(save-any-content-row :meta-title false 0 "мета тег в шаблоне")
(save-any-content-row :meta-description false 0 "мета тег в шаблоне")
(save-any-content-row :meta-keywords false 0 "мета тег в шаблоне")
(save-any-content-row :meta-subject false 0 "мета тег в шаблоне")
(save-any-content-row :meta-abstract false 0 "мета тег в шаблоне")

;; END Any content
;;..............................................................................


;;------------------------------------------------------------------------------
;; BEGIN: Politrend.kz top images
;; tag: <politrend.kz top images>
;; description: Картинки в заголовке на глдавной странице
;;------------------------------------------------------------------------------

(defentity pkz-top-images
  (table :pkz_top_images)
  (pk :id))

(defn pkz-top-images-save [vals]
  (cdbsql/common-save-for-id pkz-top-images vals))

(def pkz-top-images-select
  (-> (select* pkz-top-images)) )

;; END Politrend.kz top images
;;..............................................................................


;;**************************************************************************************************
;;* BEGIN Politrend.kz doc type
;;* tag: <politrend.kz doc type>
;;*
;;* description: Тип документа
;;*
;;**************************************************************************************************

(defentity pkz-doc-type
  (table :pkz_doc_type)
  (pk :id))

(def pkz-doc-type-select
  (-> (select* pkz-doc-type)) )

(defn pkz-doc-type-init-row [keyname description]
  (cdbsql/common-save-for-field pkz-doc-type :keyname
                                {:keyname (name keyname) :description description}))

(pkz-doc-type-init-row :notype "Без типа")
(pkz-doc-type-init-row :new "Новости-статьи")
(pkz-doc-type-init-row :service "Виды деятельности")
(pkz-doc-type-init-row :project "Проекты")
(pkz-doc-type-init-row :client "Наши клиенты")
(pkz-doc-type-init-row :device "Оборудование")

;; END Politrend.kz doc type
;;..................................................................................................



;;------------------------------------------------------------------------------
;; BEGIN: Politrend.kz docs
;; tag: <politrend.kz docs>
;; description: Документы
;;------------------------------------------------------------------------------

(defentity pkz-docs
  (table :pkz_docs)
  (pk :id)
  (belongs-to pkz-doc-type)
  (many-to-many files :pkz_docs_files)

  (prepare #(->> %
                 (cdbsql/fields-transformation-clj-time->-sql-time [:cdate :udate])
                 ))

  (transform #(->> %
                   (cdbsql/fields-transformation-clj-time-<-sql-time [:cdate :udate])
                   ))
  )

(defn pkz-docs-save [vals]
  (->> vals
       (#(-> %
             (assoc :ttitle (cw/make-translit-ru-en (:title %)))
             ))
       ((fn [vals]
          (if (:id vals)
            (assoc vals :udate (tl/local-now))
            (assoc vals :cdate (tl/local-now) :udate (tl/local-now))
            )))
       (cdbsql/common-save-for-id pkz-docs)))

(def pkz-docs-select
  (-> (select* pkz-docs)) )

(defn pkz-docs-select-for-doc-type [keyname]
  (let [select (-> pkz-docs-select (with pkz-doc-type))]
    (if (nil? keyname) select
        (-> select
            (where (= :pkz_doc_type.keyname (name keyname)))
            ))))

(defentity pkz-docs-files
  (table :pkz_docs_files)
  (pk :id)
  (belongs-to files))

(cdbsql/def-files-entitys-map-add :pkz-docs-files pkz-docs-files :pkz_docs_id)

;; END Politrend.kz docs
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: smap
;; tag: <smap entity servvice>
;; description: Настройки
;;------------------------------------------------------------------------------

(defentity smap
  (table :smap)
  (pk :id)
  (prepare #(->> %
                 (cdbsql/fields-transformation-clj-keyword->-string [:k :f])
                 ))

  (transform #(->> %
                   (cdbsql/fields-transformation-clj-keyword-<-string [:k :f])
                   )))

(defn smap-save [vals]
  (cdbsql/common-save-for-id smap vals))

(def smap-select
  (-> (select* smap)) )

(defn smap-get [k & [f]]
  (let [row (first (select smap (where (= :k (name k)))))]
    (if (nil? f) row (f row))))

(defn smap-init-row [k f v]
  (let [row (smap-get k)]
    (if (empty? row)
      (cdbsql/common-save-for-field smap :k {:k (name k) :f (name f) f v})
      row)))

(defn smap-rows-as-map []
  (->> (-> smap-select
           cdbsql/common-exec)
       (reduce #(assoc % (:k %2) %2) {}) ))

(defn smap-rows-update [rows]
  (println "Save settings: " rows "  > " (-> rows first :k name))
  (doseq [row rows]
    (update smap (set-fields row) (where (= :k (name (:k row))))))
  rows)

(smap-init-row :mail_user :v_text "user")
(smap-init-row :mail_password :v_text "password")
(smap-init-row :mail_smtp_server :v_text "mail.server.org")
(smap-init-row :mail_to :v_text "address@mail.org")

;; END smap
;;..............................................................................

;; END Entitys
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Users authentification system
;;* tag: <users auth>
;;*
;;* description: Идентификация пользователей
;;*
;;**************************************************************************************************

(defentity roles
  (pk :id))

(def roles-select
  (-> (select* roles)))

(defn init-role [rolename description]
  (cdbsql/common-save-for-field roles :rolename {:rolename (name rolename) :description description}))

(init-role :admin "Администратор")
(init-role :user "Пользователь")

(defentity users
  (pk :id)
  (many-to-many roles :users_roles))

(defn users-save [vals]
  (cdbsql/common-save-for-id users vals))

(def users-select
  (-> (select* users)) )

(defentity users-roles
  (table :users_roles))

(defn init-user [username description rolenames]
  (let [{user-id :id} (cdbsql/common-save-for-field users :username {:username (name username)
                                                                     :password (creds/hash-bcrypt "paradox")
                                                                     :description description})]
    (doseq [rolename rolenames]
      (let [{role-id :id} (-> (select roles (where (= :rolename (name rolename)))) first)
            user-role (-> (select users-roles (where (and (= :roles_id role-id) (= :users_id user-id)))) first)]
        (when (nil? user-role)
          (insert users-roles (values {:roles_id role-id :users_id user-id}))))
      )))

(init-user :admin "Администратор" [:admin :user])
(init-user :user "Пользователь" [:user])



(comment
  (def users {"root" {:username "root"
                      :password (creds/hash-bcrypt "admin_password")
                      :roles #{:admin}}
              "jane" {:username "jane"
                      :password (creds/hash-bcrypt "user_password")
                      :roles #{:user}}})
  )


(defn users-list-for-friends []
  (->> (select users (with roles))
       (reduce (fn [a {username :username roles :roles :as row}]
                 (assoc a username
                        (assoc row :roles (map #(-> % :rolename keyword) roles)))

                 )
               {})
       ))





;; END Users authentification system
;;..................................................................................................

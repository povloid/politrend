(ns politrend.web

  (:use hiccup.core)
  (:use hiccup.page)
  (:use hiccup.form)
  (:use hiccup.element)
  (:use hiccup.util)

  (:require
   ;;[net.cgrand.enlive-html :as h]
   [politrend.service :as s]

   [postal.core :as postal]

   [cemerick.friend :as friend]
   (cemerick.friend [workflows :as workflows]
                    [credentials :as creds])

   [actus.common-web :as cw]
   [actus.common-db-sql :as cdbsql]

   [clj-time.core :as tco]
   [clj-time.format :as tf]
   [clj-time.coerce :as tc]
   [clj-time.local :as tl]))


;;(when (:devel-mode env)
;;(net.cgrand.reload/auto-reload *ns*)


;; TEMPLITES ---------------------------------------------------------------------------------------

;;**************************************************************************************************
;;* BEGIN TEMPLATES
;;* tag: <templates>
;;*
;;* description: Шаблоны страниц
;;*
;;**************************************************************************************************

(def path-news (cw/path-actus "/news"))
(def path-news-edit-dialog (cw/path-actus "/news/edit-dialog"))




(def path-pkz-top-images (cw/path-actus "/pkz-top-images"))
(def path-pkz-top-images-edit-dialog (cw/path-actus "/pkz-top-images/edit-dialog"))

(def path-any-content (cw/path-actus "/any-content"))
(def path-any-content-edit-dialog (cw/path-actus "/any-content/edit-dialog"))

(def path-pkz-docs (cw/path-actus "/pkz-docs"))
(def path-pkz-docs-edit-dialog (cw/path-actus "/pkz-docs/edit-dialog"))

(def path-pkz-top-images (cw/path-actus "/pkz-top-images"))
(def path-pkz-top-images-edit-dialog (cw/path-actus "/pkz-top-images/edit-dialog"))

(def path-smap-edit-dialog (cw/path-actus "/smap/edit-dialog"))
(def path-smap-edit-dialog-a-edit (cw/path-actus "/smap/edit-dialog?actus=edit"))

(def path-users (cw/path-actus "/users"))
(def path-users-edit-dialog (cw/path-actus "/users/edit-dialog"))


;;------------------------------------------------------------------------------
;; BEGIN: NAVBAR
;; tag: <navbar>
;; description: Навигационная панель
;;------------------------------------------------------------------------------

;; Создаем основной шаблон для контента
(cw/actus-content-template
 (cw/html-navbar "Politrend CM" cw/path-actus-root
                 [;; Элемеиты, в левой части панели
                  (cw/html-navbar-link "Сайт" "/" {:target "_blank"})
                  (cw/html-navbar-menu "Контент"
                                       [;; меню
                                        (cw/html-navbar-menu-header "Документы")

                                        ;;(cw/html-navbar-menu-item "Виды деятельности" path-pkz-services)
                                        ;;(cw/html-navbar-menu-item "Документы" path-pkz-docs)
                                        ;;(cw/html-navbar-menu-item "Клиенты" path-pkz-clients)
                                        ;;(cw/html-navbar-menu-item "Новости" path-news)

                                        (cw/html-navbar-menu-item "Все.." path-pkz-docs)
                                        (for [i (-> s/pkz-doc-type-select
                                                    cdbsql/common-exec)]
                                          (cw/html-navbar-menu-item (:description i)
                                                                    (url path-pkz-docs {:pkz_doc_type_id (:id i)})))

                                        cw/html-navbar-menu-devider

                                        (cw/html-navbar-menu-header "Общий контент")

                                        (cw/html-navbar-menu-item "Разный текст..." path-any-content)

                                        cw/html-navbar-menu-devider

                                        (cw/html-navbar-menu-header "Ресурсы")

                                        (cw/html-navbar-menu-item "Картинки в заголовке" path-pkz-top-images)

                                        ])

                  ]

                 [;; Элементы по середине панели
                  (form-to {:class "navbar-form navbar-left"} [:get "/"]
                           [:input {:type "text" :class "form-control col-lg-8" :placeholder "Search"}] )
                  ]

                 [;; Элементы, привязанные к правой стороне панели
                  (cw/html-navbar-menu "Сервис"
                                       [
                                        (cw/html-navbar-menu-header "Администрирование")
                                        (cw/html-navbar-menu-item "Пользователи" path-users)
                                        (cw/html-navbar-menu-item "Настройки" path-smap-edit-dialog-a-edit)
                                        ])

                  ]))

;; END NAVBAR
;;..............................................................................




;; main page ----------------------------------------------------------------

(defn main-page [request]
  (actus-content-template
   "Смотреть пункты меню"
   ))

;;**************************************************************************************************
;;* BEGIN NEWS
;;* tag: <news>
;;*
;;* description: Новости
;;*
;;**************************************************************************************************

;;------------------------------------------------------------------------------
;; BEGIN: tabte-news
;; tag: <table-news>
;; description: Базовое описание таблици представления нововстей
;;------------------------------------------------------------------------------

(def table-news-1
  {:name :news
   :columns [
             {:field :id
              :text "№"
              :style "font-size:18px;"
              :getfn :id
              :sorter true
              }

             {:field :cdate
              :text "Дата"
              :getfn :cdate
              :sorter true
              }

             {:text "Наименование"
              :getfn #(vec [:div [:b (:keyname %)] [:br] (:top_description %) ])
              }

             {:text "Описание"
              :getfn :description
              }

             ]

   :items s/news-select
   })

;; END tabte-news
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: news page
;; tag: <news page>
;; description: Страница справочника новостей
;;------------------------------------------------------------------------------

(defn page-news [request]
  (cw/actus-in-form
   request
   {
    :add #(vector :redirect (cw/go-to-url % path-news-edit-dialog
                                          {cw/actus-keyword :add} {}))

    :edit #(vector :redirect (cw/go-to-url % path-news-edit-dialog
                                           {cw/actus-keyword :edit :id (-> % :params :id)}
                                           {:id (-> % :params :id)}))

    :del #(vector :redirect (cw/go-to-url % path-news-edit-dialog
                                          {cw/actus-keyword :del :id (-> % :params :id)}
                                          {:id (-> % :params :id)}))

    :del-dialog #(vector :response (let [{{id :id} :params} %]
                                     (html
                                      (str "Запись id: " id)
                                      [:div
                                       (cw/actus-button-wapl-warning :del "Удалить!" {:id id}) " "
                                       (cw/button-close-modal "Отмена")
                                       ])))

    :info-dialog #(vector :response (let [{{id :id} :params} %]
                                      (html (str "Запись id: " id))))
    }

   (fn [request]
     (actus-content-template
      (cw/actus-form-to request
                        :form-1 [:get path-news]
                        [
                         (hidden-field {} :id nil)

                         (cw/actus-button :add "Добавить" nil)
                         (cw/html-table-with-page-sort
                          request
                          (-> table-news-1
                              (cw/add-column
                               {:text "Действие"
                                :th-attrs {:style "width: 190px"}
                                :getfn #(let [{id :id} %]
                                          [:div
                                           (cw/actus-button-wapl :edit "Ред." {:id id} nil) " "
                                           (cw/button-show-dialog "Уд!" :dialog_delete (url path-news {cw/actus-keyword :del-dialog :id id})) " "
                                           (cw/button-show-dialog "Инф." :dialog_info (url path-news {cw/actus-keyword :info-dialog :id id}))
                                           ])
                                })
                              )
                          :0)

                         (cw/dialog-ajax :dialog_delete "Удаление записи..." "Подвал")
                         (cw/dialog-ajax :dialog_info "Информация по записи" "Подвал")

                         ;; (cw/html-table-with-page-sort request table-news-1 :9)
                         ])))))

;; END news table page
;;..............................................................................


;;------------------------------------------------------------------------------
;; BEGIN: new edit
;; tag: <new edit>
;; description: Страница редактирования новости
;;------------------------------------------------------------------------------

(defn page-news-edit-dialog [request]
  (let [form-<map>-entity [;; Связывание полей
                           {:e :id :f :id
                            :f-<-e str
                            :f->-e #(Long/parseLong %) :e-fn-rm? empty? }
                           {:e :keyname :f :keyname
                            :f-<-e str
                            :f->-e #(if (empty? %) (throw (Exception. "Пустое наименование"))  (str %)) }
                           {:e :cdate :f :cdate
                            :f-<-e #(tf/unparse cw/formatter-local-yyyy-MM-dd-HH:mm:ss %)
                            :f->-e #(tf/parse cw/formatter-local-yyyy-MM-dd-HH:mm:ss %) }
                           {:e :description :f :description
                            :f-<-e str :f->-e str }
                           ]]
    (letfn [(save-entity [entity] (s/news-save entity))]

      (cw/actus-in-form
       request
       {:close #(vector :redirect (-> % :params :ret-url))

        :add (cw/do-form-from-request->
              #(cw/try-fill-form % form-<map>-entity
                                 {:cdate (tl/local-now)})) ;; Значения по умолчанию

        :edit  (cw/do-form-from-request->
                #(let [{{id :id} :params} %]
                   (cw/try-fill-form % form-<map>-entity
                                     (cdbsql/common-find s/news-select (Long/parseLong id)) )))

        :update  (cw/do-form-from-request->
                  #(cw/try-fill-entity % form-<map>-entity {} :entity)

                  ;;;#(do (println ">>>>>>>" %) %)

                  #(let [{e :entity :as request} %]
                     (cw/try-fill-form request form-<map>-entity (save-entity e) ))

                  ;;#(/ 1 0 %)

                  #(cw/actus-add-alert % :success "Операция проведена успешно!"))

        ;;:update-and-close #(vector :redirect (-> % :params :ret-url))

        :del #(vector :redirect (let [{{id :id ret-url :ret-url} :params} %]
                                  (cdbsql/common-delete-for-id s/news (Long/parseLong id))
                                  ret-url))

        ;;:upload #(s/news-upload-and-save (-> % :headers (get "id") (Long/parseLong)) %)

        }

       (fn [{{id :id :as params} :params :as request}]
         (actus-content-template
          (cw/actus-form-to
           request :form-1 [:post path-news-edit-dialog]
           [
            (cw/actus-hidden-field params {} :id nil)

            ;;(cw/alert-page :info (str params))
            ;;(println ">>>>>>>>>" request)

            (cw/page-form-1 "LEGEND" 12

                            (cw/div-form-group
                             request "Название" 2 4
                             (cw/actus-text-field params
                                                  {:class "form-control" :placeholder "Наименование..."}
                                                  :keyname "Some name...."))

                            (cw/div-form-group
                             request "Время создания" 2 4
                             (cw/actus-text-field params
                                                  {:type "datetime" :pattern "\\d{4}-\\d{2}-\\d{2} \\d{2}-\\d{2}-\\d{2}"
                                                   :placeholder "дата в формате yyyy-MM-dd HH:mm:ss"}
                                                  :cdate ""))

                            (cw/div-form-group
                             request "Описание" 2 10
                             (cw/actus-text-area params
                                                 {:rows 3 :class "ckeditor" :placeholder "Описание..."}
                                                 :description "Some description...."))

                            (when-not (empty? id)
                              (cw/div-form-group
                               request "Файлы:" 2 10
                               (cw/actus-file-upload-list :files :news-files id 0)))

                            (when-not (empty? id)
                              (cw/div-form-group
                               request "Файлы:" 2 10
                               (cw/actus-file-upload-list :images :news-files id 1)))

                            [:p {:class "bs-component"}
                             (cw/actus-button :close "Назад" nil) " "
                             ;;(cw/actus-button :update-and-close "Принять и закрыть") " "
                             (cw/actus-button :update "Сохранить" nil) " " ;; Надо ставить пробелл так как нет переноса
                             ]

                            )
            ])))))))

;; END new edit
;;..............................................................................

;; END NEWS
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Politrend.kz settings
;;* tag: <pkz settings>
;;*
;;* description: Различные параметры сайта
;;*
;;**************************************************************************************************

(defn page-smap-edit-dialog [request]
  (let [form-<map>-entity [;; Связывание полей
                           {:e :mail_user :f :mail_user :f-<-e :v_text :f->-e (fn [v] {:v_text v :k :mail_user})}
                           {:e :mail_password :f :mail_password :f-<-e :v_text :f->-e (fn [v] {:v_text v :k :mail_password})}
                           {:e :mail_smtp_server :f :mail_smtp_server :f-<-e :v_text :f->-e (fn [v] {:v_text v :k :mail_smtp_server})}
                           {:e :mail_to :f :mail_to :f-<-e :v_text :f->-e (fn [v] {:v_text v :k :mail_to})}
                           ]]
    (cw/actus-in-form
     request
     {
      :edit  (cw/do-form-from-request->
              #(cw/try-fill-form % form-<map>-entity
                                 (s/smap-rows-as-map)))

      :update  (cw/do-form-from-request->
                #(cw/try-fill-entity % form-<map>-entity {} :entity)

                ;;#(do (println ">>>>>>>" %) %)

                #(let [{e :entity :as request} %]
                   (s/smap-rows-update (vals e))
                   request)

                #(cw/actus-add-alert % :success "Операция проведена успешно!"))
      }

     (fn [{{:as params} :params :as request}]
       (actus-content-template
        (cw/actus-form-to
         request :form-1 [:post path-smap-edit-dialog]
         [
          (cw/page-form-1 "Параметры" 12

                          (cw/div-form-group
                           request "SMTP Сервер" 2 4
                           (cw/actus-text-field params
                                                {:placeholder "Адрес SMTP сервера"}
                                                :mail_smtp_server ""))

                          (cw/div-form-group
                           request "SMTP Пользователь" 2 4
                           (cw/actus-text-field params
                                                {:placeholder "Имя пользователя..."}
                                                :mail_user "user"))

                          (cw/div-form-group
                           request "SMTP Пароль" 2 4
                           (cw/actus-text-field params
                                                {:placeholder "Пароль..."}
                                                :mail_password ""))

                          (cw/div-form-group
                           request "Адреса получателей" 2 10
                           (cw/actus-text-area params
                                               {:rows 3 :placeholder "Описание..."}
                                               :mail_to "email@address.domain"))

                          [:p {:class "bs-component"}
                           (cw/actus-button :update "Сохранить" nil)
                           ]

                          )
          ]))))))


(defn send-email [name email subject message]
  (println "Sending message: \n"
           "        name: " name
           "       email: " email
           "     subject: " subject
           "     message: " message "\n")
  (letfn [(check-par [k v er-k er-msg m]
            (if (empty? (clojure.string/trim v))
              (assoc m :success 0 er-k (or er-msg (str "Bad field: " k)))
              (assoc m er-k "")))]
    (let [{success :success :as result} (->> {:success 1}
                                             (check-par :name name :name_msg "Пустое имя")
                                             (check-par :email email :email_msg "Пустой адрес электронной посты")
                                             (check-par :subject subject :subject_msg "Пустая тема сообщения")
                                             (check-par :message message :message_msg "Пустое сообщение"))]
      (if (= success 0) (ring.util.response/response result)
          (do
            (postal/send-message {:host (s/smap-get :mail_smtp_server :v_text)
                                  :user (s/smap-get :mail_user :v_text)
                                  :pass (s/smap-get :mail_password :v_text)
                                  :ssl :yes!!!11}
                                 {:from "site@politrend.kz"
                                  :to (-> (s/smap-get :mail_to :v_text)
                                          (clojure.string/split #";"))
                                  :cc email
                                  :subject subject
                                  :body (str message "\n отправил " email) })
            (ring.util.response/response result))))))




;; END Politrend.kz settings
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Users administration
;;* tag: <users admin>
;;*
;;* description: Управление пользователями
;;*
;;**************************************************************************************************

(def table-users
  {:name :users
   :columns [
             {:field :id
              :text "№"
              :style "font-size:18px;"
              :getfn :id
              :sorter true}

             {:field :username
              :text "Ключ"
              :getfn :username
              :sorter true}

             {:text "Описание"
              :getfn #(vec [:div [:b (:description %)] [:br] (:title %) ]) }

             ]

   :items s/users-select
   })

(defn page-users-table [request]
  (cw/std-page-table-edit-entity-id
   path-users
   path-users-edit-dialog
   actus-content-template

   request
   table-users
   {}))

(defn page-users-edit [request]
  (cw/std-page-dialog-edit-entity-id
   path-users-edit-dialog
   actus-content-template

   request
   s/users
   {}
   s/users-save
   [
    {:e :username :f :username :f-<-e str
     :f->-e  #(if (empty? %) (throw (Exception. "Пустое имя пользователя"))  (str %)) }

    {:e :description :f :description :f-<-e str :f->-e str }

    {:e :password :f [:password1 :password2]
     :f-<-e (fn [a _ _] (assoc a :password1 "" :password2 ""))
     :f->-e (fn [{p1 :password1 p2 :password2 :as row} k]
              ;;(println p1 " >>> " p2)
              (if (or (not (empty? p1)) (not (empty? p2)))
                (if (not (= p1 p2))
                  (throw (Exception. "Пароль и подтверждение не совпадают!"))
                  (creds/hash-bcrypt p1))
                ))
     }

    {:e :roles :f (->> s/roles-select
                       cdbsql/common-exec
                       (map #(->> % :rolename name (str "role_") keyword)))
     :f-<-e (fn [a from-e from]
              (println "\n>>>" a from-e from)
              )
     :f->-e nil
     }

    ]
   {}
   (str "Форма редактирования параметра сайта")
   (fn [request params id]
                                        ;(println "\n>>>>" request)
     [

      (cw/div-form-group
       request "Имя пользователя" 2 4
       (cw/actus-text-field params
                            {:class "form-control" :placeholder "Заголовок описания..."}
                            :username "Some name...."))

      (cw/div-form-group
       request "Пароль" 2 4
       (cw/actus-text-field params {:type "password" :placeholder "пароль"}
                            :password1 "пароль"))
      (cw/div-form-group
       request "Подтверждение пароля" 2 4
       (cw/actus-text-field params {:type "password" :placeholder "пароль"}
                            :password2 "пароль"))

      (cw/div-form-group
       request "Описание" 2 10
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Текст описания..."}
                           :description "Some description...."))
      ])))



;; END Users administration
;;..................................................................................................



;;**************************************************************************************************
;;* BEGIN Any content refference book page
;;* tag: <any content web>
;;*
;;* description: Страница для управления всяким контентом
;;*
;;**************************************************************************************************

(def table-any-content
  {:name :any-content
   :columns [
             {:field :id
              :text "№"
              :style "font-size:18px;"
              :getfn :id
              :sorter true}

             {:field :keyname
              :text "Ключ"
              :getfn :keyname
              :sorter true}

             {:text "Описание"
              :getfn #(vec [:div [:b (:description %)] [:br] (:title %) ]) }

             ]

   :items s/any-content-select
   })

(defn page-any-content-table [request]
  (cw/std-page-table-edit-entity-id
   path-any-content
   path-any-content-edit-dialog
   actus-content-template

   request
   table-any-content
   {:add nil :del nil}))

(defn page-any-content-edit [request]
  (cw/std-page-dialog-edit-entity-id
   path-any-content-edit-dialog
   actus-content-template

   request
   s/any-content {} s/any-content-save
   [
    {:e :title :f :title :f-<-e str :f->-e str }
    {:e :body :f :body :f-<-e str :f->-e str }
    {:e :editor :f :editor :f-<-e int :f->-e int :e-fn-rm? (fn [_] true) }
    {:e :hastitle :f :hastitle :f-<-e boolean :f->-e boolean :e-fn-rm? (fn [_] true) }
    ]
   {}
   (str "Форма редактирования параметра сайта")
   (fn [request params id]
     [

      (when (:hastitle params)
        (cw/div-form-group
         request "Заголовок" 2 4
         (cw/actus-text-field params
                              {:class "form-control" :placeholder "Заголовок описания..."}
                              :title "Some name....")))

      (cw/div-form-group
       request "Описание" 2 10
       (cw/actus-text-area params
                           (merge {:rows 3 :placeholder "Текст описания..."}
                                  (if (= 1 (params :editor)) {:class "ckeditor"} nil))

                           :body "Some description...."))
      ])))


;; END Any content refference book
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Politrend.kz top slides
;;* tag: <pkz top slides>
;;*
;;* description: Картинки сверху
;;*
;;**************************************************************************************************


(def table-pkz-top-images
  {:name :pkz-top-images
   :columns [
             {:field :id
              :text "№"
              :style "font-size:18px;"
              :getfn :id
              :sorter true
              }

             {:text "Картинка"
              :getfn #(list
                       [:img {:src (str "/image" (:image %)) :alt "нет изобр." :width  "69" :height "29"}]
                       " "
                       (:description %))
              }
             ]

   :items s/pkz-top-images-select
   })


(defn page-pkz-top-images-table [request]
  (cw/std-page-table-edit-entity-id
   path-pkz-top-images
   path-pkz-top-images-edit-dialog
   actus-content-template

   request
   table-pkz-top-images))

(defn page-pkz-top-images-edit [request]
  (cw/std-page-dialog-edit-entity-id
   path-pkz-top-images-edit-dialog
   actus-content-template

   request
   s/pkz-top-images {} s/pkz-top-images-save
   [
    {:e :description :f :description :f-<-e str :f->-e str }
    {:e :image :f :image :f-<-e str :f->-e str }
    ]
   {}
   (str "Форма редактирования изображения")
   (fn [request params id]
     [
      (cw/div-form-group
       request "Изображение в заголовке" 2 10
       (cw/actus-file-upload-one-image params :image 1
                                       69 29 "Желательный формат изображения 695px × 296px"))


      (cw/div-form-group
       request "Подпись" 2 10
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Текст подписи..."}
                           :description "Some description...."))
      ])))




;; END Politrend.kz top slides
;;..................................................................................................



;;**************************************************************************************************
;;* BEGIN Politrend.kz docs
;;* tag: <politrend.kz docs>
;;*
;;* description: Документы
;;*
;;**************************************************************************************************

(def table-pkz-docs
  {:name :pkz-docs
   :columns [
             {:field :id
              :text "№"
              :style "font-size:18px;"
              :getfn :id
              :sorter true
              }

             {:text "Описание"
              :getfn #(list
                       [:b (:title %)] [:br]
                       [:p [:img {:src (str "/image" (:title_image %)) :alt "нет изобр." :style "float: left;" :width  "64" :height "64"}]
                        " " (:top_description %)])
              }

             ;; {:text "Описание"
             ;;  :getfn :description
             ;;  }

             ]

   :items s/pkz-docs-select
   })


(defn page-pkz-docs-table [{{pkz-doc-type-id :pkz_doc_type_id} :params
                            :as request}]
  (cw/std-page-table-edit-entity-id
   path-pkz-docs
   path-pkz-docs-edit-dialog
   actus-content-template
   request
   (if (empty? pkz-doc-type-id)
     table-pkz-docs
     (-> table-pkz-docs
         (cw/items-do-fn
          #(cdbsql/common-filter-by= % :pkz_doc_type_id
                                     (Long/parseLong pkz-doc-type-id))))
     )
   {
    :add (fn [{{pkz-doc-type-id :pkz_doc_type_id} :params}]
           (vector :redirect
                   (cw/go-to-url request path-pkz-docs-edit-dialog
                                 (merge {cw/actus-keyword :add}
                                        (if (empty? pkz-doc-type-id) nil
                                            {:pkz_doc_type_id pkz-doc-type-id})) {})))
    }
   {
    :before-table (cw/a-hidden-field request :pkz_doc_type_id)
    }
   ))

(defn page-pkz-docs-edit [{{doc-type-id :pkz_doc_type_id} :params :as request}]
  ;;(println "\n" request)
  (cw/std-page-dialog-edit-entity-id
   path-pkz-docs-edit-dialog
   actus-content-template

   request
   s/pkz-docs
   {:pkz_doc_type_id (or doc-type-id nil)}
   s/pkz-docs-save
   [
    {:e :title :f :title
     :f-<-e str
     :f->-e #(if (empty? %) (throw (Exception. "Пустой Заголовок"))  (str %)) }
    {:e :title_image :f :title_image :f-<-e str :f->-e str }

    {:e :cdate :f :cdate
     :f-<-e #(tf/unparse cw/formatter-local-yyyy-MM-dd-HH:mm:ss %) }
    {:e :udate :f :udate
     :f-<-e #(tf/unparse cw/formatter-local-yyyy-MM-dd-HH:mm:ss %) }

    {:e :ttitle :f :ttitle :f-<-e str }

    {:e :top_description :f :top_description :f-<-e str :f->-e str }
    {:e :description :f :description :f-<-e str :f->-e str }

    {:e :citems :f :citems :f-<-e str :f->-e str }

    {:e :pkz_doc_type_id :f :pkz_doc_type_id
     :f-<-e str :f->-e #(Long/parseLong %) }

    {:e :meta_keywords :f :meta_keywords :f-<-e str :f->-e str }
    {:e :meta_description :f :meta_description :f-<-e str :f->-e str }

    {:e :url1 :f :url1 :f-<-e str :f->-e str }
    {:e :url1flag :f :url1flag :f-<-e boolean :f->-e #(Boolean/valueOf %) }
    ]
   {}
   (str "Форма редактирования документа")
   (fn [request params id]
     [
      (cw/div-form-group
       request "Тип документа" 2 4
       (cw/actus-select params {} :pkz_doc_type_id
                        (-> s/pkz-doc-type-select cdbsql/common-exec)
                        #(vector (:description %) (-> % :id str))
                        0))

      (when-not (empty? id)
        (list
         (cw/div-form-group
          request "Параметры документа:" 2 3

          [:ul {:class "list-group"}
           [:li {:class "list-group-item"}
            [:span {:class "badge"} (:cdate params)]
            "Cоздан:"]
           [:li {:class "list-group-item"}
            [:span {:class "badge"} (:udate params)]
            "Оюновлен:"]
           ])


         (cw/div-form-group
          request "URL+" 2 10
          (cw/a-panel (cw/a-panel-body
                       (list
                        [:a {:href (url "/doc/" id "/" (:ttitle params)) :target "_blank"} "Документ на сайте..."]
                        [:br]
                        [:var (str "/doc/" id "/" (:ttitle params))]))))
         ))

      (cw/div-form-group
       request "URL" 2 4
       [:div
        (cw/actus-checkbox params {} :url1flag false "Перенаправлять на")
        (cw/actus-text-field params
                             {:placeholder "Дополнительный URL..."} :url1 "Some url....")

        ])

      (cw/div-form-group
       request "Заголовок" 2 4
       (cw/actus-text-field params
                            {:class "form-control" :placeholder "Заголовок описания..."}
                            :title "Some name...."))

      (cw/div-form-group
       request "Изображение в заголовке" 2 10
       (cw/actus-file-upload-one-image params :title_image 1 ))


      (cw/div-form-group
       request "Описание в заголовке" 2 10
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Текст описания в заголовке..."}
                           :top_description "Some description...."))


      (cw/div-form-group
       request "Структура оглавления" 2 10
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Структура оглавления (#p1 > Якорь 1; #p1 > Якорь 2; #p3 > Якорь 3;.....)"}
                           :citems "Some description...."))

      (cw/div-form-group
       request "Текст" 2 10
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Текст описания..." :class "ckeditor"}
                           :description "Some description...."))

      (cw/div-form-group
       request "Ключевые слова" 2 9
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Ключевые слова..."}
                           :meta_keywords ""))

      (cw/div-form-group
       request "Описание" 2 9
       (cw/actus-text-area params
                           {:rows 3 :placeholder "Описание..."}
                           :meta_description ""))

      (when-not (empty? id)
        (cw/div-form-group
         request "Файлы:" 2 10
         (cw/actus-file-upload-list :files :pkz-docs-files id 0)))

      (when-not (empty? id)
        (cw/div-form-group
         request "Картинки для текста:" 2 10
         (cw/actus-file-upload-list :images-for-text :pkz-docs-files id 2)))

      (when-not (empty? id)
        (cw/div-form-group
         request "Картинки галереи:" 2 10
         (cw/actus-file-upload-list :images :pkz-docs-files id 1)))

      ])))

;; END Politrend.kz docs
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN AUTH
;;* tag: <auth>
;;*
;;* description: Аутентификация пользователя
;;*
;;**************************************************************************************************

(def login-form
  [:div {:class "row"}
   [:div {:class "columns small-12"}
    [:h3 "Login"]
    [:div {:class "row"}
     [:form {:method "POST" :action "login" :class "columns small-4"}
      [:div "Username" [:input {:type "text" :name "username"}]]
      [:div "Password" [:input {:type "password" :name "password"}]]
      [:div [:input {:type "submit" :class "button" :value "Login"}]]]]]])




;; END AUTH
;;..................................................................................................





;;**************************************************************************************************
;;* BEGIN politrend.kz
;;* tag: <politrend.kz>
;;*
;;* description: Сайт
;;*
;;**************************************************************************************************

;;------------------------------------------------------------------------------
;; BEGIN: Politrend.kz common elements
;; tag: <pkz common elements>
;; description: Общие элементы сайта
;;------------------------------------------------------------------------------

(defn pkz-images [e-id caption items]
  [:div {:id e-id :class "page"}
   "<!-- page portfolio -->"
   [:h3.page_title caption]
   [:div.page_content
    ;;[:p
    ;; " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
    ;; [:ul#works_filter
    ;;  [:li [:a.selected {:data-filter "*", :href "#"} "Show All"]]
    ;;  [:li [:a {:data-filter ".css", :href "#"} "CSS"]]
    ;;  [:li [:a {:data-filter ".html_php", :href "#"} "HTML / PHP"]]
    ;;  [:li [:a {:data-filter ".js", :href "#"} "JavaScript"]]]
    [:div.clear " "]
    [:div#works
     "<!-- works -->"
     (for [i items]
       (let [image-s (str "/image" (:path i))]
         [:a
          {:href image-s, :rel "prettyPhoto[gallery]"}
          " "
          [:img.work.js {:alt "", :src image-s}]
          " "]))

     " "]
    [:div.clear " "]]])

(defn pkz-files [e-id caption items]
  [:div {:id e-id :class "page"}
   "<!-- page portfolio -->"
   [:h3.page_title caption]
   [:div.page_content
    ;;[:p
    ;; " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud\n              exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
    ;; [:ul#works_filter
    ;;  [:li [:a.selected {:data-filter "*", :href "#"} "Show All"]]
    ;;  [:li [:a {:data-filter ".css", :href "#"} "CSS"]]
    ;;  [:li [:a {:data-filter ".html_php", :href "#"} "HTML / PHP"]]
    ;;  [:li [:a {:data-filter ".js", :href "#"} "JavaScript"]]]
    [:div.clear " "]
    [:div
     "<!-- works -->"
     (for [i items]
       (let [file-s (str "/file" (:path i))]
         [:div

          [:blockquote
           [:a {:href file-s :target "_blank"} [:b (:filename i)]]
           [:br]
           (:top_description i)]]
         ))

     " "]
    [:div.clear " "]]])

;; END Politrend.kz common elements
;;..............................................................................


(defn pkz-page-template [any-map-new items content]
  (let [any-map (merge (s/get-any-as-map-k-*) any-map-new)]
    (html5 {:lang "ru"}
           [:head
            [:meta {:charset "UTF-8"}]
            [:title (:title-body any-map)]
            [:meta {:name "title" :content (:title-body any-map)}]

            [:meta {:content "width=device-width,initial-scale=1", :name "viewport"}]

            [:meta {:name "author" :content (:meta-author-body any-map)}]
            [:meta {:name "copyright" :content (:meta-copyright-body any-map)}]

            [:meta {:name "resource-type" :content "document"}]

            [:meta {:name "document-state" :content "dynamic"}]
            [:meta {:name "robots" :content "All"}]
            [:meta {:name "revizit-after" :content "2 days"}]
            [:meta {:name "revisit" :content "2"}]

            ;; -----------------------------------------------------
            [:meta {:name "description" :content (:meta-description-body any-map)}]
            [:meta {:name "keywords" :content (:meta-keywords-body any-map)}]
            [:meta {:name "subject" :content (:meta-subject-body any-map)}]
            [:meta {:name "abstract" :content (:meta-abstract-body any-map)}]
            ;; -----------------------------------------------------

            [:meta {:name "region" :content (:meta-region-body any-map)}]

            [:link {:media "screen", :href "/styles/style.css", :rel "stylesheet"}]
            ;;[:link {:href "styles/media-queries.css", :rel "stylesheet"}]
            [:link {:media "screen", :type "text/css", :href "/flex-slider/flexslider.css", :rel "stylesheet"}]
            [:link {:media "screen", :type "text/css", :rel "stylesheet", :href "/styles/prettyPhoto.css"}]
            [:link {:media "screen", :type "text/css", :rel "stylesheet", :href "/styles/tipsy.css"}]
            [:link {:type "text/css",:href "/styles/politrend.css",:rel "stylesheet"}]
            [:link {:rel "icon" :href "/images/LogoPolitrend.ico" :type "image/x-icon"}]

            [:script {:src "/scripts/jquery-1.7.1.min.js", :type "text/javascript"}]
            [:script {:src "/flex-slider/jquery.flexslider-min.js", :type "text/javascript"}]
            [:script {:type "text/javascript", :src "/scripts/jquery.prettyPhoto.js"}]
            [:script {:type "text/javascript", :src "/scripts/jquery.tipsy.js"}]
            [:script {:type "text/javascript", :src "/scripts/jquery.knob.js"}]
            [:script {:src "/scripts/jquery.isotope.min.js", :type "text/javascript"}]
            [:script {:src "/scripts/jquery.smooth-scroll.min.js", :type "text/javascript"}]
            [:script {:src "/scripts/waypoints.min.js", :type "text/javascript"}]
            [:script {:src "/scripts/setup.js", :type "text/javascript"}]

            (javascript-tag
             (cw/js-text-compressor "
$(function(){
var div_h = $('div#container').height();
var win_h = $(document).height();

if(div_h < win_h){
$('div#container').height(win_h);
}
});
"))
            ]
           [:body
            [:div#wrap
             "<!-- wrapper -->"

             [:div#box
              [:div#sidebar
               "<!-- the  sidebar -->"
               "<!-- logo -->"
               [:a#logo
                {:href "#"}
                " "
                [:img
                 {:alt "",
                  :style "width: 93%",
                  :src "/images/politrend_kz.png"}]]
               "<!-- navigation menu -->"

               (map (fn [items]
                      [:ul#navigation
                       (->> items
                            (filter #(not (nil? %)))
                            (map (fn [[href caption is-active]]
                                   [:li [(if (true? is-active) :a.active :a)
                                         {:href href } caption]]
                                   )))
                       ])

                    items)

               ]


              [:div#container
               content

               [:div.footer
                (:footer-text-body any-map)
                ]
               ]
              ]
             ;;
             ]
            [:a.gotop {:href "#top"} "В начало"]])))


(defn pkz-main-page [request]
  (let [any-map (s/get-any-as-map-k-*)]
    (pkz-page-template
     any-map
     [[["#home" "ГЛАВНАЯ" true]
       ["#news" "НОВОСТИ"]
       ["#about" "О НАС"]
       ["#portfolio" "ПРОЕКТЫ"]
       ;;["#skills" "НАШ УРОВЕНЬ"]
       ;;["#industries" "ИНДУСТРИЯ"]
       ["#myclients" "НАШИ КЛИЕНТЫ"]
       ["#contact" "КОНТАКТЫ"]]]
     (list
      "<!-- page container -->"
      [:div#home.page
       "<!-- page home -->"
       [:div.page_content
        [:div.gf-slider
         "<!-- slider -->"
         [:ul.slides
          (for [x (cdbsql/common-exec s/pkz-top-images-select)]
            [:li " " [:img {:alt "", :src (str "/image" (:image x))}]
             [:p.flex-caption (:description x)]])
          ]]
        [:div.space " "]
        [:div.clear " "]
        "<!-- services -->"

        (for [i (-> (s/pkz-docs-select-for-doc-type :service)
                    cdbsql/common-exec
                    (cw/assoc-maps-with-cycle-value :class ["one_half first" "one_half last"])
                    (cw/assoc-maps-with-cycle-value :even [1 2]))]
          (list
           [:div {:class (:class i)}
            [:div.column_content
             [:a.a2 {:href (url "/doc/" (:id i) "/" (:ttitle i)) } [:h4 (:title i)]]
             [:img.left.no_border
              {:style
               "margin-top: 10px; margin-right: 10px",
               :alt "",
               :src (str "/image" (:title_image i))}]
             [:div {:style "text-align: justify"}
              [:p
               " "
               [:small  (:top_description i) ]]]]]

           (when (= (:even i) 2) [:div.space " "])
           ))

        ;;[:div.space " "]
        [:div.clear " "]

        ]]


      [:div#news.page
       "<!-- page news -->"
       [:h3.page_title "Новости компании"]
       [:div.page_content
        (for [i (-> (s/pkz-docs-select-for-doc-type :new)
                    (cdbsql/common-sort-by :cdate :desc)
                    cdbsql/common-exec)]
          [:div {:style "text-align: justify"}
           [:a.a2 {:href (url "/doc/" (:id i) "/" (:ttitle i)) }
            [:img.no_border {:alt "", :src (str "/image" (:title_image i))
                             :style "float:left; margin:10px 10px 10px 0px;width:48px;height:48px"}]
            [:h4.page_title (:title i)]
            ]
           [:small {:style "font-style:italic;font-size:x-small"}
            (->> i :cdate (tf/unparse (tf/formatter-local "dd.MM.yyyy HH:mm:ss")))][:br]
           [:small (:top_description i)][:br][:br]
           ]
          )
        ]]

      [:div#about.page
       "<!-- page about -->"
       [:h3.page_title (:about-as-title any-map)]
       [:div.page_content
        (:about-as-body any-map)]]

      [:div#portfolio.page
       "<!-- page portfolio -->"

       [:h3.page_title (:our-projects-title any-map)]
       [:div.page_content
        (:our-projects-body any-map)
        (for  [i (-> (s/pkz-docs-select-for-doc-type :project) cdbsql/common-exec)]
          (list
           [:a.a2 {:href (url "/doc/" (:id i) "/" (:ttitle i)) } [:h4.page_title (:title i)]]
           [:div.page_content
            [:img.no_border {:alt "", :src (str "/image" (:title_image i))
                             :style "float:left; margin:10px 10px 10px 0px;"}]
            [:p (:top_description i)]]
           [:br]
           ))
        ]]

      [:div#myclients.page
       "<!-- page clients -->"
       [:h3.page_title (:our-clients-title any-map)]
       [:div.page_content
        (:our-clients-body any-map)
        [:div.space " "]
        [:div.clear " "]


        (for [i (-> (s/pkz-docs-select-for-doc-type :client) cdbsql/common-exec)]
          [:div.pkz-grid-cl
           [:a
            (if (true? (:url1flag i))
              {:href (url "/doc/" (:id i) "/" (:ttitle i)) :target "_blank"}
              {:href (url "/doc/" (:id i) "/" (:ttitle i)) })
            " "
            [:img {:alt "Altoids"
                   :src (str "/image" (:title_image i))}]]]
          )

        [:div.clear " "]]]

      [:div#contact.page
       "<!-- page contact -->"
       [:h3.page_title (:contact-as-title any-map)]
       [:div.page_content
        (:contact-as-body any-map)
        [:br]
        [:fieldset#contact_form
         [:div#msgs " "]
         [:form#cform
          {:action "", :method "post", :name "cform"}
          [:input#name
           {:placeholder "Ваше имя*",
            :name "name",
            :type "text"}]
          [:input#email
           {:placeholder "Электронный адрес почты*",
            :name "email",
            :type "text"}]
          [:input#subject
           {:placeholder "Тема*",
            :name "subject",
            :type "text"}]
          [:textarea#msg
           {:name "message"
            :placeholder "Текст сообщения*"}]
          [:button#submit.button " Отправить сообщение"]]]
        [:div.clear " "]
        ]]
      ))))

(defn pkz-main-page-doc [any-map citems content]
  (pkz-page-template
   any-map
   (conj [[["/" "&lt&lt НА ГЛАВНУЮ"]]] citems)
   (list
    "<!-- page container -->"
    [:div#home.page
     "<!-- page home -->"
     [:div.page_content
      content
      ]])))

(defn pkz-main-page-doc-1 [id]
  (let [doc (cdbsql/common-find s/pkz-docs id)
        {url1 :url1 url1flag :url1flag} doc
        images (cdbsql/common-exec (cdbsql/files-for* s/files-entitys-map s/files :pkz-docs-files id 1))
        files (cdbsql/common-exec (cdbsql/files-for* s/files-entitys-map s/files :pkz-docs-files id 0))
        citems (doc :citems)]
    (if (true? url1flag)
      (ring.util.response/redirect url1)
      (pkz-main-page-doc
       {:meta-keywords-body (:meta_keywords doc)
        :meta-description-body (:meta_description doc)
        :meta-subject-body (:title doc)
        :meta-title-body (:title doc)
        :title-body (:title doc)}
       (-> [["#home" "В начало" true]]

           (into
            (when-not (empty? citems)
              (s/content-list-from-text citems)))

           (conj
            (when-not (empty? images)
              ["#fotos" "Фото"])
            (when-not (empty? files)
              ["#files" "Файлы"])))

       (list
        [:div#about.page
         "<!-- page about -->"
         [:h3.page_title (:title doc)]
         ;;(when-let [title_image (:title_image doc)]
         ;;  [:img {:alt "", :src (str "/image" (:title_image doc))
         ;;         :style "float:left; margin:10px 10px 10px 0px;"}])
         [:div.page_content
          (:description doc)]]


        (when-not (empty? images)
          (pkz-images :fotos "Фото"  images))

        (when-not (empty? files)
          (pkz-files :files "Файлы" files))

        ;;(str files)
        )))))



;; END politrend.kz
;;..................................................................................................

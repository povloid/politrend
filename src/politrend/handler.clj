(ns politrend.handler
  (:use compojure.core)

  (:require [clojure.java.io :as io]
            [ring.middleware.reload :refer (wrap-reload)] ;; reload temlates
            [ring.middleware.json :as json]

            [compojure.handler :as handler]
            [compojure.route :as route]
            [politrend.web :as w]
            [politrend.service :as s]

            [actus.common-web :as cw]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])
            )
  )

;;**************************************************************************************************
;;* BEGIN Init web aplication
;;* tag: <init web application>
;;*
;;* description: Инициализация вебприложения
;;*
;;**************************************************************************************************

(defn init []
  (println "INIT SYSTEM")

  ;;(print "STARTING SCHEDULLER -> ")
  ;; Some code.....
  ;;(print "[ok]")

  )

;; END Init web aplication
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN URL ROUTES
;;* tag: <url route system>
;;*
;;* description: Управление вызовами
;;*
;;**************************************************************************************************


(defroutes app-routes
  ;;(GET "/" [] (w/main-page))
  ;;(GET "/" [] "Hello World!")
  ;;(GET "/" [] (main-page))
  ;;(GET "/" [] (test-html "Привет"))

  (GET "/text/:text" [text] text)


  ;; FILES & IMAGES -----------------------------------------------------------------
  (cw/create-routes-for-file-upload* s/files-entitys-map
                                     s/files
                                     1024
                                     "/opt/politrend-data"
                                     "" ""
                                     (* 20 10 1024 1024))
  ;; --------------------------------------------------------------------------------

  (GET "/robots.txt" [] (s/get-any-content-body :robots.txt))

  ;; AUTH ----------------------------------------------------

  (GET "/login" req
       (hiccup.page/html5 w/login-form))
  (GET "/logout" req
       (friend/logout* (ring.util.response/redirect (str (:context req) "/"))))
  (GET "/requires-authentication" req
       (friend/authenticated "Thanks for authenticating!"))
  (GET "/role-user" req
       (friend/authorize #{:user} "You're a user!"))
  (GET "/role-admin" req
       (friend/authorize #{:admin} "You're an admin!"))

  ;; ACTUS ----------------------------------------------------

  (ANY cw/path-actus-root request (friend/authorize #{:admin} (w/main-page request)))

  (ANY w/path-pkz-top-images request (friend/authorize #{:admin} (w/page-pkz-top-images-table request)))
  (ANY w/path-pkz-top-images-edit-dialog request (friend/authorize #{:admin} (w/page-pkz-top-images-edit request)))

  (ANY w/path-any-content request (friend/authorize #{:admin} (w/page-any-content-table request)))
  (ANY w/path-any-content-edit-dialog request (friend/authorize #{:admin} (w/page-any-content-edit request)))

  (ANY w/path-news request (friend/authorize #{:admin} (w/page-news request)))
  (ANY w/path-news-edit-dialog request (friend/authorize #{:admin} (w/page-news-edit-dialog request)))

  (ANY w/path-pkz-docs request (friend/authorize #{:admin} (w/page-pkz-docs-table request)))
  (ANY w/path-pkz-docs-edit-dialog request (friend/authorize #{:admin} (w/page-pkz-docs-edit request)))

  (ANY w/path-smap-edit-dialog request (friend/authorize #{:admin} (w/page-smap-edit-dialog request)))

  (ANY w/path-users request (friend/authorize #{:admin} (w/page-users-table request)))
  (ANY w/path-users-edit-dialog request (friend/authorize #{:admin} (w/page-users-edit request))  )

  (ANY "/mock-returl" {{ret-url :ret-url :as params} :params :as request}
       (ring.util.response/redirect ret-url))

  (ANY "/" request (w/pkz-main-page request))


  (ANY "/doc/:id*" [id] (w/pkz-main-page-doc-1 (Long/parseLong id)))

  (POST "/send_message" [name email subject message]
        (w/send-email name email subject message))

  (route/resources "/")
  (route/not-found "Not Found"))


;; END URL ROUTES
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Ring middleware
;;* tag: <ring web midleware>
;;*
;;* description: Веб функционал RING
;;*
;;**************************************************************************************************

(defn spec-middle
  "Дополнительняа промежуточная функция"
  [handler]
  (fn [request]
    #_(println request)
    #_(use 'politrend.web :reload)

    (handler request)))


(def users {"root" {:username "root"
                     :password (creds/hash-bcrypt "23rwe5scf")
                     :roles #{:admin}}
            "politrend" {:username "politrend"
                     :password (creds/hash-bcrypt "slavakpss!!!")
                     :roles #{:admin}}})


(def site
  (-> app-routes
      #_(friend/authenticate
       {:allow-anon? true
        :login-uri "/login"
        :default-landing-uri "/"
        :unauthorized-handler #(-> (hiccup.page/html5 [:h2 "You do not have sufficient privileges to access " (:uri %)])
                                   ring.util.response/response
                                   (ring.util.response/status 401))
        :credential-fn #(creds/bcrypt-credential-fn (s/users-list-for-friends) %)
        :workflows [(workflows/interactive-form)]})

      (friend/authenticate {:credential-fn (partial creds/bcrypt-credential-fn users)
                            :workflows [(workflows/interactive-form)]})

      spec-middle
      json/wrap-json-response))


(def app
  (handler/site site))


;; END Ring middleware
;;..................................................................................................

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-04-24 01:59:22 ALMT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 192 (class 3079 OID 11791)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 192
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 28796)
-- Name: any_content; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE any_content (
    id integer NOT NULL,
    keyname character varying(20),
    title text,
    description text,
    body text,
    editor integer DEFAULT 0 NOT NULL,
    hastitle boolean DEFAULT false NOT NULL
);


--
-- TOC entry 171 (class 1259 OID 28804)
-- Name: any_content_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE any_content_files (
    any_content_id integer NOT NULL,
    files_id integer NOT NULL
);


--
-- TOC entry 172 (class 1259 OID 28807)
-- Name: any_content_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE any_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 172
-- Name: any_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE any_content_id_seq OWNED BY any_content.id;


--
-- TOC entry 173 (class 1259 OID 28809)
-- Name: files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE files (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    urlpath text,
    filename character varying(50),
    description text,
    typegroup integer DEFAULT 0 NOT NULL,
    top_description text
);


--
-- TOC entry 174 (class 1259 OID 28816)
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 174
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- TOC entry 175 (class 1259 OID 28818)
-- Name: news; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE news (
    id integer NOT NULL,
    cdate timestamp with time zone NOT NULL,
    keyname character varying(255) NOT NULL,
    top_description text,
    description text,
    titleimageurl character varying(255),
    meta_description character varying(255),
    meta_keywords character varying(255),
    meta_subject character varying(255)
);


--
-- TOC entry 176 (class 1259 OID 28824)
-- Name: news_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE news_files (
    news_id integer NOT NULL,
    files_id integer NOT NULL
);


--
-- TOC entry 177 (class 1259 OID 28827)
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 177
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE news_id_seq OWNED BY news.id;


--
-- TOC entry 178 (class 1259 OID 28829)
-- Name: pkz_doc_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pkz_doc_type (
    id integer NOT NULL,
    keyname character varying(50),
    description text
);


--
-- TOC entry 179 (class 1259 OID 28835)
-- Name: pkz_doc_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pkz_doc_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 179
-- Name: pkz_doc_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pkz_doc_type_id_seq OWNED BY pkz_doc_type.id;


--
-- TOC entry 180 (class 1259 OID 28837)
-- Name: pkz_docs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pkz_docs (
    id integer NOT NULL,
    cdate timestamp with time zone NOT NULL,
    udate timestamp with time zone NOT NULL,
    title text NOT NULL,
    ttitle text,
    title_image text,
    top_description text,
    citems text,
    description text,
    pkz_doc_type_id integer,
    meta_keywords text,
    meta_description text,
    url1 text,
    url1flag boolean
);


--
-- TOC entry 181 (class 1259 OID 28843)
-- Name: pkz_docs_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pkz_docs_files (
    pkz_docs_id integer NOT NULL,
    files_id integer NOT NULL
);


--
-- TOC entry 182 (class 1259 OID 28846)
-- Name: pkz_docs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pkz_docs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 182
-- Name: pkz_docs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pkz_docs_id_seq OWNED BY pkz_docs.id;


--
-- TOC entry 183 (class 1259 OID 28848)
-- Name: pkz_top_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pkz_top_images (
    id integer NOT NULL,
    image text,
    description text
);


--
-- TOC entry 184 (class 1259 OID 28854)
-- Name: pkz_top_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pkz_top_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 184
-- Name: pkz_top_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pkz_top_images_id_seq OWNED BY pkz_top_images.id;


--
-- TOC entry 185 (class 1259 OID 28856)
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    rolename character varying(20),
    description text
);


--
-- TOC entry 186 (class 1259 OID 28862)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 186
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- TOC entry 187 (class 1259 OID 28864)
-- Name: smap; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE smap (
    id integer NOT NULL,
    k character varying(20) NOT NULL,
    f character varying(10),
    v_text text
);


--
-- TOC entry 188 (class 1259 OID 28870)
-- Name: smap_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE smap_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 188
-- Name: smap_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE smap_id_seq OWNED BY smap.id;


--
-- TOC entry 189 (class 1259 OID 28872)
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(20),
    password character varying(60),
    description text,
    block boolean
);


--
-- TOC entry 190 (class 1259 OID 28878)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 190
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 191 (class 1259 OID 28880)
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users_roles (
    users_id integer NOT NULL,
    roles_id integer NOT NULL
);


--
-- TOC entry 1938 (class 2604 OID 28883)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY any_content ALTER COLUMN id SET DEFAULT nextval('any_content_id_seq'::regclass);


--
-- TOC entry 1940 (class 2604 OID 28884)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- TOC entry 1941 (class 2604 OID 28885)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY news ALTER COLUMN id SET DEFAULT nextval('news_id_seq'::regclass);


--
-- TOC entry 1942 (class 2604 OID 28886)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_doc_type ALTER COLUMN id SET DEFAULT nextval('pkz_doc_type_id_seq'::regclass);


--
-- TOC entry 1943 (class 2604 OID 28887)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_docs ALTER COLUMN id SET DEFAULT nextval('pkz_docs_id_seq'::regclass);


--
-- TOC entry 1944 (class 2604 OID 28888)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_top_images ALTER COLUMN id SET DEFAULT nextval('pkz_top_images_id_seq'::regclass);


--
-- TOC entry 1945 (class 2604 OID 28889)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- TOC entry 1946 (class 2604 OID 28890)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY smap ALTER COLUMN id SET DEFAULT nextval('smap_id_seq'::regclass);


--
-- TOC entry 1947 (class 2604 OID 28891)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 1953 (class 2606 OID 28900)
-- Name: any_content_files_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY any_content_files
    ADD CONSTRAINT any_content_files_pk PRIMARY KEY (any_content_id, files_id);


--
-- TOC entry 1949 (class 2606 OID 28902)
-- Name: any_content_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY any_content
    ADD CONSTRAINT any_content_pk PRIMARY KEY (id);


--
-- TOC entry 1951 (class 2606 OID 28904)
-- Name: any_content_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY any_content
    ADD CONSTRAINT any_content_uk1 UNIQUE (keyname);


--
-- TOC entry 1955 (class 2606 OID 28906)
-- Name: file_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT file_pk PRIMARY KEY (id);


--
-- TOC entry 1963 (class 2606 OID 28908)
-- Name: news_files_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY news_files
    ADD CONSTRAINT news_files_pk PRIMARY KEY (news_id, files_id);


--
-- TOC entry 1959 (class 2606 OID 28910)
-- Name: news_keyname_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY news
    ADD CONSTRAINT news_keyname_key UNIQUE (keyname);


--
-- TOC entry 1961 (class 2606 OID 28912)
-- Name: news_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- TOC entry 1965 (class 2606 OID 28914)
-- Name: pkz_doc_type_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pkz_doc_type
    ADD CONSTRAINT pkz_doc_type_pk PRIMARY KEY (id);


--
-- TOC entry 1967 (class 2606 OID 28916)
-- Name: pkz_doc_type_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pkz_doc_type
    ADD CONSTRAINT pkz_doc_type_uk1 UNIQUE (keyname);


--
-- TOC entry 1971 (class 2606 OID 28918)
-- Name: pkz_docs_files_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pkz_docs_files
    ADD CONSTRAINT pkz_docs_files_pk PRIMARY KEY (pkz_docs_id, files_id);


--
-- TOC entry 1969 (class 2606 OID 28920)
-- Name: pkz_docs_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pkz_docs
    ADD CONSTRAINT pkz_docs_pk PRIMARY KEY (id);


--
-- TOC entry 1973 (class 2606 OID 28922)
-- Name: pkz_top_images_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pkz_top_images
    ADD CONSTRAINT pkz_top_images_pk PRIMARY KEY (id);


--
-- TOC entry 1975 (class 2606 OID 28924)
-- Name: roles_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pk PRIMARY KEY (id);


--
-- TOC entry 1977 (class 2606 OID 28926)
-- Name: roles_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_uk1 UNIQUE (rolename);


--
-- TOC entry 1979 (class 2606 OID 28928)
-- Name: smap_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY smap
    ADD CONSTRAINT smap_pk PRIMARY KEY (id);


--
-- TOC entry 1981 (class 2606 OID 28930)
-- Name: smap_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY smap
    ADD CONSTRAINT smap_uk1 UNIQUE (k);


--
-- TOC entry 1983 (class 2606 OID 28932)
-- Name: users_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- TOC entry 1987 (class 2606 OID 28934)
-- Name: users_roles_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_pk PRIMARY KEY (users_id, roles_id);


--
-- TOC entry 1985 (class 2606 OID 28936)
-- Name: users_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_uk1 UNIQUE (username);


--
-- TOC entry 1956 (class 1259 OID 28937)
-- Name: files_idx1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX files_idx1 ON files USING btree (path);


--
-- TOC entry 1957 (class 1259 OID 28938)
-- Name: files_idx2; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX files_idx2 ON files USING btree (filename);


--
-- TOC entry 1988 (class 2606 OID 28939)
-- Name: any_content_files_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY any_content_files
    ADD CONSTRAINT any_content_files_fk1 FOREIGN KEY (any_content_id) REFERENCES any_content(id);


--
-- TOC entry 1990 (class 2606 OID 28944)
-- Name: news_files_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY news_files
    ADD CONSTRAINT news_files_fk1 FOREIGN KEY (news_id) REFERENCES news(id);


--
-- TOC entry 1991 (class 2606 OID 28949)
-- Name: news_files_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY news_files
    ADD CONSTRAINT news_files_fk2 FOREIGN KEY (files_id) REFERENCES files(id);


--
-- TOC entry 1989 (class 2606 OID 28954)
-- Name: news_files_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY any_content_files
    ADD CONSTRAINT news_files_fk2 FOREIGN KEY (files_id) REFERENCES files(id);


--
-- TOC entry 1993 (class 2606 OID 28959)
-- Name: pkz_docs_files_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_docs_files
    ADD CONSTRAINT pkz_docs_files_fk1 FOREIGN KEY (pkz_docs_id) REFERENCES pkz_docs(id);


--
-- TOC entry 1994 (class 2606 OID 28964)
-- Name: pkz_docs_files_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_docs_files
    ADD CONSTRAINT pkz_docs_files_fk2 FOREIGN KEY (files_id) REFERENCES files(id);


--
-- TOC entry 1992 (class 2606 OID 28969)
-- Name: pkz_docs_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pkz_docs
    ADD CONSTRAINT pkz_docs_fk1 FOREIGN KEY (pkz_doc_type_id) REFERENCES pkz_doc_type(id);


--
-- TOC entry 1995 (class 2606 OID 28974)
-- Name: users_roles_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_fk1 FOREIGN KEY (users_id) REFERENCES users(id);


--
-- TOC entry 1996 (class 2606 OID 28979)
-- Name: users_roles_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_fk2 FOREIGN KEY (roles_id) REFERENCES roles(id);


-- Completed on 2014-04-24 01:59:26 ALMT

--
-- PostgreSQL database dump complete
--

